﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Bridge.Models.ClassRepository
{
    public class DocumentXML
    {
        public string workflowname { get; set; }
        public string user_name { get; set; }
        public string autorizzazioni { get; set; }
        public List<UserInput> UserInputValues { get; set; }
        public DocumentXML()
        {
            this.autorizzazioni = "";
            this.UserInputValues = new List<UserInput>();
            this.user_name = "";
            this.workflowname = "";
        }
    }
    public class UserInput
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public UserInput()
        {
            this.Name = "";
            this.Value = "";
        }
    }
}