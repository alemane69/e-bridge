﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using E_Bridge.Models.DAL;
namespace E_Bridge.Models.BLL
{
    public class RoleBLL
    {
        public bool isUserInRole(string UserName,string Role)
        {
            bool ret = false;
            using (var db = new DatiEntities())
            {
                var q = from tab in db.AspNetUsers where tab.UserName == UserName select tab;
                if(q.Count()>0)
                {
                    if (q.First().AspNetRoles.Where(x => x.Name == Role).Count() > 0)
                        ret = true;
                }
            }
            return ret;
        }
        public List<AspNetRoles> getUserRoles(string UserName)
        {
            List<AspNetRoles> oRet = new List<DAL.AspNetRoles>();
            using (var db = new DatiEntities())
            {
                var q = from tab in db.AspNetUsers where tab.UserName == UserName select tab;
                if (q.Count() > 0)
                {
                    foreach(var el in q.First().AspNetRoles)
                    {
                        oRet.Add(el);
                    }
                }
            }
            return oRet;
        }
        public bool addUserRole(string UserName,string RoleName)
        {
            bool ret = false;
            using (var db = new DatiEntities())
            {
                var q = from tab in db.AspNetRoles where tab.Name == RoleName select tab;
                if (q.Count() > 0)
                {
                    AspNetRoles _role = q.First();
                    if(!this.isUserInRole(UserName,RoleName))
                    {
                        var qU = from tab in db.AspNetUsers where tab.UserName == UserName select tab;
                        if (qU.Count() > 0)
                        {
                            qU.First().AspNetRoles.Add(_role);
                            db.SaveChanges();
                        }
                        else
                            return ret;
                    }
                }
                else
                    return ret;
            }
            return ret;
        }

    }
}