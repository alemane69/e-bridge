﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.text.pdf.parser;
using System.Xml;

namespace E_Bridge.Models.BLL
{
    public class DocumentBLL
    {
        public string GetTextFromPDF()
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader("F:\\E-Bridge\\Progetto_EBRIDGEDF.pdf"))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
            }

            return text.ToString();
        }
        public FileStream getPdfStream()
        {
            var fileStream = new FileStream("F:\\E-Bridge\\Progetto_EBRIDGEDF.pdf",
                                     FileMode.Open,
                                     FileAccess.Read
                                   );
            return fileStream;
        }
        public string RemoveStopWord(string DocContent)
        {
            string ret = DocContent;

            return DocContent;
        }
        public void StoreFileInfo(string DocRoot,DateTime LastUpdate)
        {
            foreach (var file in Directory.GetFiles(@"" + DocRoot, "*.pdf", SearchOption.AllDirectories))
            {
                //in file c'è nome file
                FileInfo oFileInfo = new FileInfo(file);
                DateTime _lstChange = oFileInfo.LastWriteTime;
                TimeSpan _diff = LastUpdate -_lstChange;
                if(_diff.TotalSeconds>0)
                {
                    //file modificato, faccio tutte le operazioni
                    string _fileNameNoType = System.IO.Path.GetFileNameWithoutExtension(file);
                    //string _dir = System.IO.Path.GetDirectoryName(file);
                    string _xmlFileName = _fileNameNoType + ".xml";
                    string _xmlFileNameFull = System.IO.Path.Combine(oFileInfo.DirectoryName, _xmlFileName);
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(_xmlFileNameFull);
                    Models.ClassRepository.DocumentXML oXML = new ClassRepository.DocumentXML();
                    XmlNodeList nodeList = xmldoc.GetElementsByTagName("workflowname");
                    oXML.workflowname = nodeList[0].InnerText;
                    XmlNodeList nodeList1 = xmldoc.GetElementsByTagName("user_name");
                    oXML.user_name = nodeList1[0].InnerText;
                    XmlNodeList nodeList2 = xmldoc.GetElementsByTagName("autorizzazioni");
                    oXML.autorizzazioni = nodeList2[0].InnerText;

                    XmlElement root = xmldoc.DocumentElement;
                    XmlNodeList nodes = root.SelectNodes("user_input"); // You can also use XPath here
                    foreach (XmlNode node in nodes)
                    {
                        // use node variable here for your beeds

                        foreach (XmlNode _el in node.ChildNodes)
                        {
                            Models.ClassRepository.UserInput _tmp = new ClassRepository.UserInput();
                            _tmp.Name = _el.Attributes["name"].InnerText;
                            _tmp.Value = _el.InnerText;
                            oXML.UserInputValues.Add(_tmp);
                        }
                    }

                }
            }
        }
    }
}