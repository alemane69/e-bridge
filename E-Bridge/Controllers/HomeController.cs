﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Bridge.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            Models.BLL.DocumentBLL oDocBLL = new Models.BLL.DocumentBLL();
            oDocBLL.StoreFileInfo("F:\\Progetti\\progetti 2015\\E-Bridge\\DocumentRepository", DateTime.Now);
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult pdf()
        {
            Models.BLL.DocumentBLL oDocBLL = new Models.BLL.DocumentBLL();
            var fsResult = new FileStreamResult(oDocBLL.getPdfStream(), "application/pdf");
            return fsResult;
        }
    }
}