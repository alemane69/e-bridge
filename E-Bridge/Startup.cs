﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E_Bridge.Startup))]
namespace E_Bridge
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
