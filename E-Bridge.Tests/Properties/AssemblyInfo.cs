﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Le informazioni generali relative a un assembly sono controllate dal seguente
// set di attributi. Modificare i valori di questi attributi per modificare le informazioni
// associate a un assembly.
[assembly: AssemblyTitle("E_Bridge.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("E_Bridge.Tests")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Se si imposta il valore di ComVisible su falso, i tipi nell'assembly non sono più visibili
// dai componenti COM. Se è necessario accedere al tipo nell'assembly da
// COM, impostare su true l'attributo ComVisible per tale tipo.
[assembly: ComVisible(false)]

// Se il progetto viene esposto a COM, il GUID seguente verrà utilizzato per creare l'ID di typelib
[assembly: Guid("3bd0fab3-b6ac-4caf-9ec3-fa0d97e3dcde")]

// Le informazioni sulla versione di un assembly sono costituite dai quattro valori seguenti:
//
//      Versione principale
//      Versione secondaria
//      Numero build
//      Revisione
//
// È possibile specificare tutti i valori o lasciare i valori predefiniti per Revisione e Numeri build
// utilizzando l'asterisco (*) come illustrato di seguito:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
